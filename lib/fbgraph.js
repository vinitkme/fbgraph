/**
 * Module dependencies
 */


var fs = require('fs')
  , request = require('superagent')
  , graphUrl = 'https://graph.facebook.com/'
  , mongoose = require('mongoose')



var Fbgraph = module.exports = function (config) {
  if (config) {
    this.config = config
  }
}


// Graph Api Methods

Fbgraph.prototype = {


/**
 * User Profile
 */

userProfile: function (uid) {
  request
    .get(graphUrl+uid)
    .set('access_token', userA.token)
    .end(function (res){
      return res.body
    })
}



/**
 * User Friends
 */

userFriends: function (uid) {
  request
    .get(graphUrl+uid+'/friends')
    .set('access_token', userA.token)
    .end(function (res) {
      return res.body
    })
}

/**
 * User Relationships
 */

userRelations: function (uid) {
  request
    .get(graphUrl+uid+'/relations')
    .set('access_token', userA.token)
    .end(function (res) {
      return res.body
    })
}

/**
 * Messages
 */

messages: function (uid) {
  request
    .get(graphUrl+uid+'/messages')
    .set('access_token', userA.token)
    .end(function (res) {
      return res.body
    })
}

/**
 * User Photos
 */

photos: function (uid) {
  request
    .get(graphUrl+uid+'/photos')
    .set('access_token', userA.token)
    .end(function (res) {
      return res.body
    })
}

/**
 * User Albums
 */

userAlbums: function (uid) {
  request
    .get(graphUrl+uid+'/photos')
    .set('access_token', userA.token)
    .end(function (res) {
      return res.body
    })
}

/**
 * Page Albums
 */

pageAlbums: function (page_uid) {
  request
    .get(graphUrl+page_uid+'/photos')
    .set('access_token', userA.token)
    .end(function (res) {
      return res.body
    })
}

/**
 * Friends Likes
 */

friendLikes: function (uid) {
  request
    .get(graphUrl+page_uid+'/likes')
    .set('access_token', userA.token)
    .end(function (res) {
      return res.body
    })
}

/**
 * Page names of the Friends likes
 */





/**
 * Page name of the user's like.
 */


}
